import React, {useContext} from 'react';
import "./Navbar.scss"
import {Context} from "../../index";
import {Avatar, Button, IconButton} from "@material-ui/core";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {
    ArrowDropDown,
    Help,
    Menu,
    Search,
    ShoppingBasket, Stars,
    ThumbUp
} from "@material-ui/icons";
import {Link, useHistory} from "react-router-dom";
import {ADMIN_ROUTE,SHOP_ROUTE} from "../../utils/consts";
import {observer} from "mobx-react-lite";

const Navbar = observer(() => {
    const {user} = useContext(Context)
    const history = useHistory()

    const logout = () => {
        user.setUser({})
        user.setIsAuth(false)
        history.push("/login")
    }

    return (
        <div className="header">
            <div className="header__left">
                <IconButton>
                    <Menu/>
                </IconButton>
                <Link to={SHOP_ROUTE} style={{textDecoration: "none"}}>
                    <img
                        src="https://thumbs.dreamstime.com/b/store-icon-market-retail-shop-vector-commercial-print-media-web-any-type-design-projects-185043213.jpg"
                        alt=""
                    />
                    <span className="header__left-logo">TechnoStore</span>
                </Link>
            </div>

            <div className="header__middle">
                <Search />
                <input placeholder="Search device" type="text" />
                <ArrowDropDown className="header__inputCaret" />
            </div>

            <div className="header__right">
                <IconButton>
                    <Help />
                </IconButton>
                <IconButton>
                    <Stars />
                </IconButton>
                <IconButton>
                    <ThumbUp />
                </IconButton>
                <IconButton>
                    <ShoppingBasket />
                </IconButton>

                 {user.isAuth && <Button onClick={() => history.push(ADMIN_ROUTE)} color="secondary" className="admin__panel">Админ панель</Button>}
                {user.isAuth &&  <Avatar
                    className="header__right-avatar"
                />}
                <IconButton>
                    <ExitToAppIcon onClick={logout}/>
                </IconButton>
            </div>
        </div>

    );
});

export default Navbar;