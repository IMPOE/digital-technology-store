import React, {useContext, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import {Context} from "../../index";
import {observer} from "mobx-react-lite";

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            marginTop: theme.spacing(2),
        },
    },
}));

const PaginationRounded = observer(() =>  {
    const classes = useStyles();
    const {device} = useContext(Context)
    const pageCount = Math.ceil(device.totalCount/device.limit)

    const pages = []

    for( let i = 0; i < pageCount; i++){
        pages.push(i + 1)
        console.log(pages)
    }
    return (
        <div className={classes.root} style={{padding: "2rem 0 ", display: "flex", justifyContent: "center", alignItems:"center"}}>
            <Pagination  variant="outlined" shape="rounded"  count={pages.length}   onClick={
                (e) => {
                    device.setPage(e.target.outerText)
                }
            }/>
        </div>
    );
})


export default PaginationRounded;