import React, {useContext} from 'react';
import {Context} from "../../index";

const Select = ({setSelectedInput}) => {
    const {device} = useContext(Context)
    return (
        <div>
            <select id = "dropdown" onChange={(e) => setSelectedInput(e.target.value)}  >
                <option value="" selected disabled hidden>Выбрать тип</option>
                {device.types.map((e) =>  <option value={e.id}>{e.name}</option>)}
            </select>
        </div>
    );
};

export default Select;