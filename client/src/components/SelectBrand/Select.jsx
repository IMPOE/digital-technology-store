import React, {useContext} from 'react';
import {Context} from "../../index";

const SelectBrand = ({setSelectedInputBrand}) => {
    const {device} = useContext(Context)
    return (
        <div>
            <select id = "dropdown" onChange={(e) => setSelectedInputBrand(e.target.value)} >
                <option value="" selected disabled hidden>Выбрать бренд</option>
                {device.brands.map((e) =>  <option  value={e.id}>{e.name}</option>)}
            </select>
        </div>
    );
};

export default SelectBrand;
