import React, {useContext, useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Button from "@material-ui/core/Button";
import {Context} from "../../index";
import MultipleSelect from "../Select/Select";
import SelectBrand from "../SelectBrand/Select";
import {createBrand, createDevice, createType, fetchBrands, fetchTypes} from "../../http/DeviceAPI";




function getModalStyle() {
    const top = 50 ;
    const left = 50 ;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const useStyles = makeStyles((theme) => ({
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

const SimpleModal = ({handleOpen,setOpen,handleClose,open,params}) => {
    const classes = useStyles();
    const {device} = useContext(Context)
    const [modalStyle] = useState(getModalStyle);
    const [info, setInfo] = useState([])
    const [typeInput , setTypeInput] = useState("")
    const [brandInput , setBrandInput] = useState("")

    // device input
    const [devicePrice,setDevicePrice] = useState(0)
    const [deviceName,setDeviceName] = useState("")
    const [file,setFile] = useState(null)
    const [selectedInputBrand,setSelectedInputBrand] = useState([])
    const [selectedInput,setSelectedInput] = useState([])

    //

    const selectFile = (e) => {
        setFile(e.target.files[0])
    }

    const AddInfo = () => {
        setInfo([...info,{
            title: "",
            description: "",
            number: Date.now()
        }])


    }

    useEffect(() => {
        fetchTypes().then(data => device.setTypes(data))
        fetchBrands().then(data => device.setBrands(data))
    },[])
    const RemoveInfo = (number) => {
        setInfo(info.filter(i => i.number !== number))
    }


    const addType = () => {
        createType({name: typeInput}).then(data => setTypeInput(""))
        handleClose()
    }

    const addBrand = () => {
        createBrand({name: brandInput}).then(data => setBrandInput(""))
        handleClose()
    }

    const changeInfo = (key,value,number) => {
        setInfo(info.map( i => i.number === number ? {...i,[key]: value}: i))
    }

    const addDevice = () => {
        const formData = new FormData
        formData.append("name", deviceName)
        formData.append("price", `${devicePrice}`)
        formData.append("img", file)
        formData.append("brandId", selectedInputBrand)
        formData.append("typeId", selectedInput)
        formData.append("info", JSON.stringify(info))

        createDevice(formData).then(data => handleClose())
    }
    const body = (
        <div style={modalStyle} className={classes.paper} style={{overflowX: "scroll", maxHeight: "90vh", left:"30rem", top: "5rem"}}>
            <h2 style={{margin: "20px 0"}}>Добавить{" "}{params}</h2>
            <span>Тип</span>
            {params === "DEVICE" && <MultipleSelect setSelectedInput={setSelectedInput}/>}
            <span>Бренд</span>
            {params === "DEVICE" && <SelectBrand setSelectedInputBrand={setSelectedInputBrand} />}
            {params === "DEVICE" && <input value={deviceName} onChange={(e) =>  setDeviceName(e.target.value)} placeholder="Введите название устройтва" style={{width: "90%",margin: "10px 0", borderTop: "1px solid black", padding: "10px"}} type="text"/>}
            {params === "DEVICE" && <input value={devicePrice} onChange={(e)=> setDevicePrice(Number(e.target.value))} placeholder="Введите стоимость устройтва" style={{width: "90%",margin: "10px 0", borderTop: "1px solid black", padding: "10px"}} type="number"/>}
            {params === "DEVICE" && <input onChange={selectFile} style={{width: "90%",margin: "10px 0", borderTop: "1px solid black", padding: "10px"}} type="file"/>}
            {params === "DEVICE" && <Button onClick={ AddInfo} variant="outline-dark" color="primary">Добавить новое свойтво</Button>}
            { info.map((i) => <div key={i.number} style={{border: "1px solid gray", display: "flex", flexDirection: "column", alignItems: "center"}}>
                <input value={i.title} onChange={(e) => changeInfo("title", e.target.value,i.number)} style={{width: "90%", margin: "5px"}} placeholder="Введите название" type="text"/>
                <input value={i.description} onChange={(e) => changeInfo("description", e.target.value,i.number)}  style={{width: "90%", marginBottom: "5px"}} placeholder="Введите описание" type="text"/>
                <Button onClick={() => RemoveInfo(i.number)}  variant="ouline" color="secondary">Удалить</Button>
            </div>)}
            {params === "DEVICE" && <Button onClick={addDevice} color="primary">Добавить</Button>}
            {params === "DEVICE" && <Button onClick={handleClose} color="secondary">Закрыть</Button>}

            {params === "BRAND" && <input value={brandInput} onChange={(e) => setBrandInput(e.target.value)} style={{width: "90%",margin: "10px 0", borderTop: "1px solid black", padding: "10px"}} type="text"/>}
            {params === "BRAND" && <Button onClick={addBrand} color="primary">Добавить</Button>}
            {params === "BRAND" && <Button onClick={handleClose} color="secondary">Закрыть</Button>}



            {params === "TYPE" && <input value={typeInput} onChange={(e) => setTypeInput(e.target.value)} style={{width: "90%",margin: "10px 0", borderTop: "1px solid black", padding: "10px"}} type="text"/>}
            {params === "TYPE" && <Button color="primary" onClick={addType}>Добавить</Button>}
            {params === "TYPE" && <Button onClick={handleClose} color="secondary">Закрыть</Button>}

            <SimpleModal />
        </div>
    );

    return (
        <div>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                {body}
            </Modal>
        </div>
    );
}

export default SimpleModal;