import React, {useContext, useEffect, useState} from 'react';
import {BrowserRouter} from "react-router-dom";
import AppRouter from "./components/AppRouter";
import {observer} from "mobx-react-lite";
import {Context} from "./index";
import Navbar from "./components/Navbar/Navbar";
import {check} from "./http/UserAPI";

const App = observer(() => {
    const {user} = useContext(Context)
    const [loading, setLoading] = useState(true)


    useEffect(() => {
        check().then(data => {
            user.setUser(true)
            user.setIsAuth(true)
        }).finally(() => setLoading(false))
    }, [])

    return (
        <BrowserRouter>
            {user.isAuth && <Navbar />}
            <AppRouter />
        </BrowserRouter>
    );
});

export default App;