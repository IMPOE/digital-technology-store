import React from 'react';
import "./BrandBar.scss"
import {observer} from "mobx-react-lite";
const BrandBar = observer(({name,onClick,id,selected}) => {
    return (
        <div className="brand" onClick={onClick} style={id === selected ? {backgroundColor: "grey", color: "white"}: {}}>
            <p className="brand__group" style={id === selected ? { color: "white"}: {}}>{name}</p>
        </div>
    );
});

export default BrandBar;