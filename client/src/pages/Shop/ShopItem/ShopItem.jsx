import React, {useContext} from 'react';
import "./ShopItem.scss"
import {Star} from "@material-ui/icons";
import {useHistory} from "react-router-dom";
import {DEVICE_ROUTE} from "../../../utils/consts";
import Tooltip from '@material-ui/core/Tooltip';
import {Context} from "../../../index";
const ShopItem = ({device}) => {
    const history = useHistory()
    const {user} = useContext(Context)

    function addRation (e) {
        console.log(user.user)
        console.log(e.target.id)
    }


    return (
        <div className="shop__item" >
            <div className="shop__item-header" >
                <span className="shop__item-name" onClick={() => history.push( DEVICE_ROUTE + "/" + device.id)}>{device?.name}</span>
            </div>
            <div className="shop__item-rating">
                {device?.rating}
                <Star />
                <div className="star__container">
                    <Tooltip title="1" placement="top">
                        <Star id={1}  onClick={(e) => addRation(e)}/>
                    </Tooltip>
                    <Tooltip  title="2" placement="top">
                        <Star id={2}  onClick={(e) => addRation(e)}/>
                    </Tooltip>
                    <Tooltip  title="3" placement="top">
                        <Star id={3} onClick={(e) => addRation(e)}/>
                    </Tooltip>
                    <Tooltip  title="4" placement="top">
                        <Star id={4} onClick={(e) => addRation(e)}/>
                    </Tooltip>
                    <Tooltip title="5" placement="top">
                        <Star id={5}  onClick={(e) => addRation(e)}/>
                    </Tooltip>
                </div>
            </div>
            <img onClick={() => history.push( DEVICE_ROUTE + "/" + device.id)} className="shop__item-img" src={"http://localhost:5000/" + device.img} alt={device?.name}/>
            <p className="shop__item-price">
                Цена:{" "}{device?.price}
            </p>
        </div>
    );
};

export default ShopItem;