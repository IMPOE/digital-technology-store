import React from 'react';
import "./ListGroup.scss"
const ListGroup = ({name,onClick,id,selected}) => {
    return (
        <div className="list" onClick={onClick} style={id === selected ? {backgroundColor: "grey", color: "white"}: {}}>
            <p className="list__group" style={id === selected ? { color: "white"}: {}}>{name}</p>
        </div>
    );
};

export default ListGroup;