import React, {useContext, useEffect} from 'react';
import "./Shop.scss"
import ShopItem from "./ShopItem/ShopItem";
import {Context} from "../../index";
import {observer} from "mobx-react-lite";
import ListGroup from "./SideBar/ListGroup";
import BrandBar from "./BrandBar/BrandBar";
import {fetchBrands, fetchDevices, fetchTypes} from "../../http/DeviceAPI";
import PaginationRounded from "../../components/Paggination/Paggination";
const Shop = observer(() => {
    const {device} = useContext(Context)

    useEffect(() => {
        fetchTypes().then(data => device.setTypes(data))
        fetchBrands().then(data => device.setBrands(data))
        fetchDevices(null,null,1,3).then(data => {
            device.setDevices(data.rows)
            device.setTotalCount(data.count)
        })
    },[])


    useEffect(() => {
        fetchDevices(device.selectedType.id, device.selectedBrand.id, device.page, 2).then(data => {
            console.log(data.rows)
            device.setDevices(data.rows)
            device.setTotalCount(data.count)
        })
    }, [device.page, device.selectedType, device.selectedBrand,])

    return (
        <div className="shop" >
            <div className="shop__sidebar">
                    {device.types.map((type) => <ListGroup
                        selected={device.selectedType.id}
                        id={type.id}
                        onClick={()=> device.setSelectedType(type)}
                        key={type.id}
                        name={type.name}/>)}
            </div>
            <div className="shop__main">
                <div className="shop__main-header">
                    {device.brands.map((brand) => <BrandBar
                    selected={device.selectedBrand?.id}
                    id={brand.id}
                    onClick={()=> device.setSelectedBrand(brand)}
                    key={brand.id}
                    name={brand.name}/>)}</div>
                <div className="shop__main-content">
                    {device.devices.map((device) => <ShopItem
                        device={device}
                        key={device.id}
                        />)}
                </div>
                <PaginationRounded/>
            </div>
        </div>
    );
});

export default Shop;