import React, {useState} from 'react';
import Button from "@material-ui/core/Button";
import "./Admin.scss"
import SimpleModal from "../../components/Modals/Modal";
const Admin = () => {
    const [open, setOpen] = useState(false);
    const [params, setParams] = useState("")
    const handleOpen = async (param) => {
        await setParams(param)
        await setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };


    return (
        <div className="admin">
            <SimpleModal handleOpen={handleOpen} setOpen={setOpen} handleClose={handleClose} open={open} params={params}/>
            <div className="admin_button">
                <Button onClick={() => handleOpen("TYPE")}>Добавить Тип</Button>
            </div>
            <div className="admin_button">
                <Button onClick={() => handleOpen("BRAND")}>Добавить Бренд</Button>
            </div>
            <div className="admin_button">
                <Button onClick={() => handleOpen("DEVICE")}>Добавить Устройство</Button>
            </div>

        </div>
    );
};

export default Admin;