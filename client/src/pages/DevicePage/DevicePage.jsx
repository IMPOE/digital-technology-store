import React, {useEffect, useState} from 'react';
import {Star} from "@material-ui/icons";
import Button from "@material-ui/core/Button";
import "./DevicePage.scss"
import {useParams} from "react-router-dom";
import {fetchOneDevice} from "../../http/DeviceAPI";

const DevicePage = () => {
    const [device, setDevice] = useState({info: []})
    const {id} = useParams()

    useEffect(() => {
        fetchOneDevice(id)
            .then(data => setDevice(data))
    },[])
    return (
        <div className="device">
            <div className="device_header">
                <h4 className="device_header-name">{device?.name}</h4>
                <div className="device_header-container">
                    <div className="device_header-star">
                        <img className="device_header-img" src={"http://localhost:5000/" +device?.img} alt=""/>
                        <div className="device_header-starContainer">
                            <p className="rating_star">Рейтинг :</p>
                            <Star className="device_header-svg"/>
                            <p>{device?.rating}</p>
                        </div>
                    </div>
                    <div className="device_header-cart">
                        <div className="advertising">
                            <img src="https://st2.depositphotos.com/7358038/12220/v/600/depositphotos_122207074-stock-illustration-tag-with-percent-discount-icon.jpg" alt=""/>
                                <div>
                                    <p className="advertising_text" >Акция! Выгода до 5000 грн на смартфоны Samsung Galaxy S21/S21 Ultra по программе «Обмен
                                        с выгодой» + оплата частями!</p>
                                    <p>С 3 августа по 16 августа 2021</p>
                                </div>
                            <Button className="advertising_button">Узнать детали</Button>
                        </div>
                        <div className="price_container">
                            <p className="price">Цена: </p>
                            <p className="price">{device?.price}</p>
                        </div>
                        <Button className="addToCart">Добавить в корзину</Button>
                    </div>
                </div>
            </div>
            <div className="device_description">
                <h1>Характеристики</h1>
                {device.info.map((item,index) => <p style={{backgroundColor: index % 2 === 0 ? "lightgray" : "" }} key={item?.id}>{item?.title}: <span>{item?.description}</span></p>  )}
            </div>
        </div>
    );
};

export default DevicePage;